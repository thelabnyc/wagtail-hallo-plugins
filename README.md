# Wagtail Hallo Plugins

A short collection of hallo plugins that I like to reuse.

Initially started from https://thirdbearsolutions.com/blog/stateful-hallojs-plugins-wagtail/

# Installation

1. `pip install wagtail-hallo-plugins`
2. Add `wagtail_hallo_plugins` to `INSTALLED_APPS` in `settings.py`
3. Add `WAGTAIL_HALLO_PLUGINS` in `settings.py` and enable the plugins you wish to use.

For example if you only wanted superscript add

```
WAGTAIL_HALLO_PLUGINS = [
    'superscript',
]
```

# Plugins

- superscript
- subscript

# I want more plugins

Please submit a merge request and ensure your plugin can be enabled in settings.
All plugins should be disabled by default.

If you have an idea on how to unit test them...please let me know.